# DataAnalytics_1

# Список (создение)
user_info = ['Konst', 'Repev', 150000.0, 20]
# Список (получить элемент)
user_info = salaries[0]
user_info = salaries[-1]
# Список (добавить элемент)
user_info.append('Voronov')
# Длина списка
len(user_info)

###########################################

# Циклы, 
for age in user_ages:
	print (age)

# Условия
if stag < 2:
    position = 'junior'
elif 2 <= stag <= 5:  # или stag >=2 and stag <= 5
    position = 'middle'
else:
    position = 'senior'

###########################################

# Форматирование строк
status = '{} {} is {}'
status = status.format(user_name, user_family, position)
status = f'{user_name} {user_family} is {position}'

###########################################

# Операции
% - остаток от деления

###########################################

# Создание процедуры
def temp_to_celcius(Fahrenheit):
    return(((Fahrenheit-32)*5.0)/(9.0))
	
###########################################

# Библиотека для работы с таблицами
import pandas as pd
# Создание DataFrame
df = pd.DataFrame({'column1': ['a', 'b', 'c'], 'columnN': [1, 2, 3]})
# Чтение данных из файла в DataFrame
df = pd.read_csv("C:\Jupyter-notebook\DataAnalytics_1\SW.BAND_2/2_taxi_nyc.csv.zip", encoding='utf-8', sep=",", compression = 'zip')
# Запись DataFrame в файл
df.to_csv("2_taxi_nyc.csv.csv", index=False)
# Размер DataFrame
df.shape
# Чтение n строк начала DataFrame
df.head(n=5)
# Чтение n строк конца DataFrame
df.tail(n=5)
# Список колонок
df.columns